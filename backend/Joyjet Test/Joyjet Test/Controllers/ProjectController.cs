﻿using Joyjet_Test.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using System.Data;
using Newtonsoft.Json;
using Joyjet_Test.ViewModel;
using System;
using System.Dynamic;
using static Joyjet_Test.Useful.LowercaseJsonSerializer;

namespace Joyjet_Test.Controllers
{
    public class ProjectController : ApiController
    {
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("project/level1")]
        public JsonResult Post([FromBody]Level1VM level1VM)
        {
            List<ReturnedCart> carts = new List<ReturnedCart>();

            foreach (Cart cart in level1VM.Carts)
            {
                ReturnedCart response = new ReturnedCart();

                foreach (Item item in cart.Items)
                    response.Total += level1VM.Articles.Where(x => x.Id == item.Article_id).Single().Price * item.Quantity;

                response.Id = cart.Id;
                carts.Add(response);
            }
            dynamic obj = new ExpandoObject();
            obj.carts = carts;

            var settings = new JsonSerializerSettings();
            settings.ContractResolver = new LowercaseContractResolver();
            var json = JsonConvert.SerializeObject(obj,settings);
            return new JsonResult()
            {
                Data = json,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                
            };
        }

        [System.Web.Http.Route("project/level2")]
        public JsonResult Post([FromBody]Level2VM level2VM)
        {
            List<ReturnedCart> carts = new List<ReturnedCart>();

            foreach (Cart cart in level2VM.Carts)
            {
                ReturnedCart response = new ReturnedCart();

                foreach (Item item in cart.Items)
                    response.Total += level2VM.Articles.Where(x => x.Id == item.Article_id).Single().Price * item.Quantity;

                //After get the price of the cart we have to check how much will cost its fee.
                //I've ordered first to always get the fewer min_price... After that we have this simple if
                foreach (DeliveryFee deliveryFee in level2VM.Delivery_fees.OrderBy(x => x.Eligible_transaction_volume.Min_price))
                    if (response.Total >= deliveryFee.Eligible_transaction_volume.Min_price && response.Total < (deliveryFee.Eligible_transaction_volume.Max_price ?? int.MaxValue)) //?? -> if left value is null, return right value
                    {
                        response.Total += deliveryFee.Price;
                        break;
                    }

                response.Id = cart.Id;
                carts.Add(response);
            }
            dynamic obj = new ExpandoObject();
            obj.carts = carts;

            var settings = new JsonSerializerSettings();
            settings.ContractResolver = new LowercaseContractResolver();
            var json = JsonConvert.SerializeObject(obj, settings);
            return new JsonResult()
            {
                Data = json,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,

            };
        }

        [System.Web.Http.Route("project/level3")]
        public JsonResult Post([FromBody]Level3VM level3VM)
        {
            List<ReturnedCart> carts = new List<ReturnedCart>();

            foreach (Cart cart in level3VM.Carts)
            {
                ReturnedCart response = new ReturnedCart();

                foreach (Item item in cart.Items)
                {
                    Article article = level3VM.Articles.Where(x => x.Id == item.Article_id).Single();
                    int price = article.Price;
                    Discount discount = level3VM.Discounts.Where(x => x.Article_id == article.Id).SingleOrDefault();
                    if (discount != null)
                    {
                        if (discount.Type == "amount")
                            price -= discount.Value;
                        else if (discount.Type == "percentage")
                            price -= (int)Math.Ceiling(price * discount.Value / 100.00); //Looks like it needs to be "ceilinged" to reach the desired output...
                        else
                            throw new Exception("Wrong parameter at discount type");
                    }
                    response.Total += price * item.Quantity;

                }

                //After get the price of the cart we have to check how much will cost its fee.
                //I've ordered first to always get the fewer min_price... After that we have this simple if
                foreach (DeliveryFee deliveryFee in level3VM.Delivery_fees.OrderBy(x => x.Eligible_transaction_volume.Min_price))
                    if (response.Total >= deliveryFee.Eligible_transaction_volume.Min_price && response.Total < (deliveryFee.Eligible_transaction_volume.Max_price ?? int.MaxValue)) //?? -> if left value is null, return right value
                    {
                        response.Total += deliveryFee.Price;
                        break;
                    }

                response.Id = cart.Id;
                carts.Add(response);
            }
            dynamic obj = new ExpandoObject();
            obj.carts = carts;

            var settings = new JsonSerializerSettings();
            settings.ContractResolver = new LowercaseContractResolver();
            var json = JsonConvert.SerializeObject(obj, settings);
            return new JsonResult()
            {
                Data = json,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,

            };
        }
    }
}
