﻿namespace Joyjet_Test.Models
{
    public class EligibleTransactionVolume
    {
        public int Min_price { get; set; }
        public int? Max_price { get; set; }
    }
}
