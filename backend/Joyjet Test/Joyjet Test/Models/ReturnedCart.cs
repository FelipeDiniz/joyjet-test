﻿namespace Joyjet_Test.Models
{
    public class ReturnedCart
    {
        public int Id { get; set; }
        public int Total { get; set; }
    }
}
