﻿namespace Joyjet_Test.Models
{
    public class Discount
    {
        public int Article_id { get; set; }
        public string Type { get; set; }
        public int Value { get; set; }
    }
}
