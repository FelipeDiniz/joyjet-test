﻿namespace Joyjet_Test.Models
{
    public class DeliveryFee
    {
        public EligibleTransactionVolume Eligible_transaction_volume { get; set; }
        public int Price { get; set; }
    }
}
