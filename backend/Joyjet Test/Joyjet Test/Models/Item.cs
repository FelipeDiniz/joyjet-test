﻿namespace Joyjet_Test.Models
{
    public class Item
    {
        public int Article_id { get; set; }
        public int Quantity { get; set; }
    }
}
