﻿using Joyjet_Test.Models;
using System.Collections.Generic;

namespace Joyjet_Test.ViewModel
{
    public class Level2VM : Level1VM
    {
        public List<DeliveryFee> Delivery_fees { get; set; }
    }
}
