﻿using Joyjet_Test.Models;
using System.Collections.Generic;

namespace Joyjet_Test.ViewModel
{
    public class Level3VM : Level2VM
    {
        public List<Discount> Discounts { get; set; }
    }
}
