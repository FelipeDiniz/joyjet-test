﻿using Joyjet_Test.Models;
using System.Collections.Generic;

namespace Joyjet_Test.ViewModel
{
    public class Level1VM
    {
        public List<Article> Articles { get; set; }
        public List<Cart> Carts { get; set; }
    }
}
