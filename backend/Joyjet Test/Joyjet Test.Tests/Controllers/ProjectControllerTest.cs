﻿using Joyjet_Test.Controllers;
using Joyjet_Test.Models;
using Joyjet_Test.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Joyjet_Test.Tests.Controllers
{
    [TestClass]
    public class ProjectControllerTest
    {
        [TestMethod]
        public void PostTest()
        {
            // Arrange
            ProjectController controller = new ProjectController();

            //expected json result
            string expectedJson = "{\"carts\":[{\"id\":1,\"total\":2000},{\"id\":2,\"total\":1400},{\"id\":3,\"total\":0}]}";

            #region case 1: success

            //View Model
            Level1VM level1 = GetData();

            // Json
            JsonResult result = controller.Post(level1) as JsonResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expectedJson, result.Data);
            #endregion
        }

        private Level1VM GetData()
        {
            Level1VM level1VM = new Level1VM();

            #region Instances
            level1VM.Articles = new List<Article>();
            level1VM.Carts = new List<Cart>();
            List<Item> item1 = new List<Item>();
            List<Item> item2 = new List<Item>();
            #endregion

            #region Set Itens
            item1.Add(new Item()
            {
                Article_id = 1,
                Quantity = 6
            });
            item1.Add(new Item()
            {
                Article_id = 2,
                Quantity = 2
            });
            item1.Add(new Item()
            {
                Article_id = 4,
                Quantity = 1
            });

            item2.Add(new Item()
            {
                Article_id = 2,
                Quantity = 1
            });
            item2.Add(new Item()
            {
                Article_id = 3,
                Quantity = 3
            });

            #endregion

            #region Set Articles
            level1VM.Articles.Add(new Article()
            {
                Id = 1, Name = "water", Price=100
            });
            level1VM.Articles.Add(new Article()
            {
                Id = 2,
                Name = "honey",
                Price = 200
            });
            level1VM.Articles.Add(new Article()
            {
                Id = 3,
                Name = "mango",
                Price = 400
            });
            level1VM.Articles.Add(new Article()
            {
                Id = 4,
                Name = "tea",
                Price = 1000
            });

            #endregion

            #region Set Carts
            level1VM.Carts.Add(new Cart()
            {
                Id = 1,
                Items = item1
            });

            level1VM.Carts.Add(new Cart()
            {
                Id = 2,
                Items = item2
            });

            level1VM.Carts.Add(new Cart()
            {
                Id = 3,
                Items = new List<Item>()
            });

            #endregion

            return level1VM;
        }

        [TestMethod]
        public void SecondPostTest()
        {
            // Arrange
            ProjectController controller = new ProjectController();

            //expected json result
            string expectedJson = "{\"carts\":[{\"id\":1,\"total\":2000},{\"id\":2,\"total\":1800},{\"id\":3,\"total\":800}]}";
            #region case 1: Success
            Level2VM level2 = GetLvl2Data();

            // Json
            JsonResult result = controller.Post(level2) as JsonResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expectedJson, result.Data);
            #endregion
        }

        private Level2VM GetLvl2Data()
        {
            Level2VM level2VM = new Level2VM();

            #region instances
            level2VM.Articles = new List<Article>();
            level2VM.Carts = new List<Cart>();
            level2VM.Delivery_fees = new List<DeliveryFee>();
            #endregion

            #region Get Articles and Carts
            level2VM.Articles = GetData().Articles;
            level2VM.Carts = GetData().Carts;
            #endregion

            #region Set Delivery
            level2VM.Delivery_fees.Add(new DeliveryFee()
            {
                Eligible_transaction_volume = new EligibleTransactionVolume()
                {
                    Min_price = 0,
                    Max_price = 1000
                },
                Price = 800
            });

            level2VM.Delivery_fees.Add(new DeliveryFee()
            {
                Eligible_transaction_volume = new EligibleTransactionVolume()
                {
                    Min_price = 1000,
                    Max_price = 2000
                },
                Price = 400
            });

            level2VM.Delivery_fees.Add(new DeliveryFee()
            {
                Eligible_transaction_volume = new EligibleTransactionVolume()
                {
                    Min_price = 2000,
                    Max_price = null
                },
                Price = 0
            });

            #endregion

            return level2VM;
        }

        [TestMethod]
        public void ThirdPostTest()
        {
            // Arrange
            ProjectController controller = new ProjectController();

            //expected json result
            string expectedJson = "{\"carts\":[{\"id\":1,\"total\":2350},{\"id\":2,\"total\":1775},{\"id\":3,\"total\":1798},{\"id\":4,\"total\":1083},{\"id\":5,\"total\":1196}]}";

            #region case 1: Success

            Level3VM level3 = GetLvl3Data();

            // Json
            JsonResult result = controller.Post(level3) as JsonResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expectedJson, result.Data);

            #endregion

        }

        private Level3VM GetLvl3Data()
        {
            Level3VM level3VM = new Level3VM();

            #region instances
            level3VM.Articles = new List<Article>();
            level3VM.Carts = new List<Cart>();
            level3VM.Delivery_fees = new List<DeliveryFee>();
            level3VM.Discounts = new List<Discount>();

            List<Item> item1 = new List<Item>();
            List<Item> item2 = new List<Item>();
            List<Item> item3 = new List<Item>();
            List<Item> item4 = new List<Item>();
            List<Item> item5 = new List<Item>();

            #endregion

            #region Set Itens

            //item1
            item1.Add(new Item()
            {
                Article_id = 1,
                Quantity = 6
            });
            item1.Add(new Item()
            {
                Article_id = 2,
                Quantity = 2
            });
            item1.Add(new Item()
            {
                Article_id = 4,
                Quantity = 1
            });

            //item2
            item2.Add(new Item()
            {
                Article_id = 2,
                Quantity = 1
            });
            item2.Add(new Item()
            {
                Article_id = 3,
                Quantity = 3
            });

            //item3
            item3.Add(new Item()
            {
                Article_id = 5,
                Quantity = 1
            });
            item3.Add(new Item()
            {
                Article_id = 6,
                Quantity = 1
            });

            //item4
            item4.Add(new Item()
            {
                Article_id = 7,
                Quantity = 1
            });

            //item5
            item5.Add(new Item()
            {
                Article_id = 8,
                Quantity = 3
            });

            #endregion

            #region Set Articles
            level3VM.Articles.Add(new Article()
            {
                Id = 1,
                Name = "water",
                Price = 100
            });
            level3VM.Articles.Add(new Article()
            {
                Id = 2,
                Name = "honey",
                Price = 200
            });
            level3VM.Articles.Add(new Article()
            {
                Id = 3,
                Name = "mango",
                Price = 400
            });
            level3VM.Articles.Add(new Article()
            {
                Id = 4,
                Name = "tea",
                Price = 1000
            });
            level3VM.Articles.Add(new Article()
            {
                Id = 5,
                Name = "ketchup",
                Price = 999
            });
            level3VM.Articles.Add(new Article()
            {
                Id = 6,
                Name = "mayonnaise",
                Price = 999
            });
            level3VM.Articles.Add(new Article()
            {
                Id = 7,
                Name = "fries",
                Price = 378
            });
            level3VM.Articles.Add(new Article()
            {
                Id = 8,
                Name = "ham",
                Price = 147
            });
            #endregion

            #region Get Carts

            level3VM.Carts.Add(new Cart()
            {
                Id = 1,
                Items = item1
            });
            level3VM.Carts.Add(new Cart()
            {
                Id = 2,
                Items = item2
            });
            level3VM.Carts.Add(new Cart()
            {
                Id = 3,
                Items = item3
            });
            level3VM.Carts.Add(new Cart()
            {
                Id = 4,
                Items = item4
            });
            level3VM.Carts.Add(new Cart()
            {
                Id = 5,
                Items = item5
            });


            #endregion

            #region Set Delivery

            level3VM.Delivery_fees.Add(new DeliveryFee()
            {
                Eligible_transaction_volume = new EligibleTransactionVolume()
                {
                    Min_price = 0,
                    Max_price = 1000
                },
                Price = 800
            });

            level3VM.Delivery_fees.Add(new DeliveryFee()
            {
                Eligible_transaction_volume = new EligibleTransactionVolume()
                {
                    Min_price = 1000,
                    Max_price = 2000
                },
                Price = 400
            });

            level3VM.Delivery_fees.Add(new DeliveryFee()
            {
                Eligible_transaction_volume = new EligibleTransactionVolume()
                {
                    Min_price = 2000,
                    Max_price = null
                },
                Price = 0
            });

            #endregion

            #region Set Discounts

            level3VM.Discounts.Add(new Discount
            {
                Article_id = 2,
                Type = "amount",
                Value = 25
            });

            level3VM.Discounts.Add(new Discount
            {
                Article_id = 5,
                Type = "percentage",
                Value = 30
            });

            level3VM.Discounts.Add(new Discount
            {
                Article_id = 6,
                Type = "percentage",
                Value = 30
            });

            level3VM.Discounts.Add(new Discount
            {
                Article_id = 7,
                Type = "percentage",
                Value = 25
            });

            level3VM.Discounts.Add(new Discount
            {
                Article_id = 8,
                Type = "percentage",
                Value = 10
            });

            #endregion

            return level3VM;
        }
    }
}
